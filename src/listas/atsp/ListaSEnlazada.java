
package listas.atsp;

import javax.swing.JOptionPane;

/*
    LISTA SIMPLEMENTE ENLAZADA DE DATOS ENTEROS NEGATIVOS
*/
class CNodo {
	int dato;
	CNodo siguiente;
        
	public CNodo()	{
            siguiente = null;
	}
}

class CLista {
    
    CNodo cabeza;
    
    public CLista()
    {
            cabeza = null;
    }

    public void InsertarDato(int dat) {
        CNodo NuevoNodo;
        CNodo antes, luego;
        NuevoNodo = new CNodo();
        NuevoNodo.dato=dat;
        int ban=0;
        if (dat < 0){
        if (cabeza == null){ //lista esta vacia
            NuevoNodo.siguiente=null;
            cabeza = NuevoNodo;
        }
        else {
            if (dat<cabeza.dato) //dato va antes de cabeza
            {
                    NuevoNodo.siguiente=cabeza;
                    cabeza = NuevoNodo;
            }
                else {  
                        antes=cabeza;
                        luego=cabeza;
                        while (ban==0)
                        {
                            if (dat>=luego.dato) 
                            {
                                antes=luego;
                                luego=luego.siguiente;
                            }
                            if (luego==null)
                            {
                                ban=1;
                            }
                            else 
                            {
                                    if (dat<luego.dato){
                                        ban=1;
                                    }
                            }
                        }
                        antes.siguiente=NuevoNodo;
                        NuevoNodo.siguiente=luego;
                }
        }
    }else{
            JOptionPane.showMessageDialog(null,"No ha ingresado un numero negativo, por favor ingrese de nuevo");
        }
    }
        
    public void EliminarDato(int dat) {
        CNodo antes,luego;
        int ban=0;
        if (Vacia()) {
            JOptionPane.showMessageDialog(null,"Lista vacía ");
        }
        else {  if (dat<cabeza.dato) {
                    JOptionPane.showMessageDialog(null,"El número no existe en la lista ");
                }
                else {
                        if (dat==cabeza.dato) {
                            cabeza=cabeza.siguiente;
                        }
                        else {  antes=cabeza;
                                luego=cabeza;
                                while (ban==0) {
                                    if (dat>luego.dato) {
                                        antes=luego;
                                        luego=luego.siguiente;
                                    }
                                    else ban=1;
                                    if (luego==null) {
                                        ban=1;
                                    }
                                    else {
                                            if (luego.dato==dat) 
                                                ban=1;
                                    }
                                }
                                if (luego==null) {
                                   JOptionPane.showMessageDialog(null,"El número no existe en la Lista ");
                                }
                                else {
                                        if (dat==luego.dato) {
                                            antes.siguiente=luego.siguiente;
                                        }
                                        else 
                                           JOptionPane.showMessageDialog(null,"El número no existe en la Lista ");
                                }
                        }
                }
        }
    }

    public boolean Vacia() {
        return(cabeza==null);
    }

    public void Imprimir() {
        CNodo Temporal;
        String salida= "";
        Temporal=cabeza;
        if (!Vacia()) {
            while (Temporal!=null) {
                salida = salida + Temporal.dato +", ";
                Temporal = Temporal.siguiente;
            }
            JOptionPane.showMessageDialog(null,salida);
        }
        else
           JOptionPane.showMessageDialog(null, "Lista vacía");
    }
}

public class ListaSEnlazada {
    public static void main(String args[]) {
        
        CLista objLista= new CLista();
        int control;
        do {
            String salida="====Menú Manejo Lista====\n"+"1- Insertar elemento\n"+"2- Eliminar elemento\n"+"3- Imprimir lista\n";
            salida=salida+"4- Salir\n";
            String entra = JOptionPane.showInputDialog(null, salida);
            int op = Integer.parseInt(entra);
            control = op;
            switch(op){
                        case 1: int valor = Integer.parseInt(JOptionPane.showInputDialog("Digite un número negativo para la lista"));
                                objLista.InsertarDato(valor);
                                break;
                                
                        case 2: int eliminar = Integer.parseInt(JOptionPane.showInputDialog("Digite un número a eliminar"));
                                objLista.EliminarDato(eliminar);
                                break;
                                
                        case 3: objLista.Imprimir();
                                break;
                                
                        case 4: System.exit(op);
                                break;
                }
            
        }while(control<4);
    }
    
    
}